<?php
/*
 * @file hotel_booking.ca.inc
 * Conditional Action hooks for hotel_booking module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
* Implementation of hook_ca_predicate().
*/
function hotel_booking_ca_predicate() {
  // Alter the availability when a payment is received
  // and the order is paid in full
  $configurations['hotel_booking_payment_received'] = array(
    '#title' => t('Reduce room availability status on full payment'),
    '#description' => t('Only happens when a payment is entered and the balance is <= $0.00.'),
    '#class' => 'payment',
    '#trigger' => 'uc_payment_entered',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_payment_condition_order_balance',
          '#title' => t('If the balance is less than or equal to $0.00.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => FALSE,
            'balance_comparison' => 'less_equal',
          ),
        ),
        array(
          '#name' => 'uc_order_status_condition',
          '#title' => t('If the order status is not already Payment Received.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => TRUE,
            'order_status' => 'payment_received',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'hotel_booking_reduce_availability',
        '#title' => t('Reduce the room availability.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'order_status' => 'payment_received',
        ),
      ),
    ),
  );

   // Increase availability in case of cancellation
  $configurations['hotel_booking_order_cancelled'] = array(
    '#title' => t('Increase room availability on order cancellation.'),
    '#description' => t('When order is cancelled, room availability should be increased.'),
    '#class' => 'payment',
    '#trigger' => 'uc_order_status_update',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_order_status_condition',
          '#title' => t('If the order status is cancelled'),
          '#argument_map' => array(
            'order' => 'updated_order',
          ),
          '#settings' => array(
            'order_status' => 'canceled',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'hotel_booking_increase_availability',
        '#title' => t('Increase the room availability.'),
        '#argument_map' => array(
          'order' => 'updated_order',
        ),
      ),
    ),
  );

  return $configurations;
}

/**
 * Implementation of hook_ca_action().
 */
function hotel_booking_ca_action() {
  $order_arg = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );

  $actions['hotel_booking_reduce_availability'] = array(
    '#title' => t('Reduce the availability of rooms'),
    '#category' => t('Order'),
    '#callback' => 'hotel_booking_alter_availability',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );
  $actions['hotel_booking_increase_availability'] = array(
    '#title' => t('Increase the availability of rooms'),
    '#category' => t('Order'),
    '#callback' => 'hotel_booking_alter_availability',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  return $actions;
}

/**
 * Callback for ca action to reduce availability of
 * a room type associated with an order
 * @param $order object uc_order order object
 * @see hotel_booking_ca_action
*/
function hotel_booking_alter_availability($order) {
 
  switch ($order->order_status) {
    case 'canceled':
      $message = 'Processing cancelled booking(s)';
      $sign = "+";
      break;
    default:
      $message = 'Processing booking(s)';
      $sign = "-";
  }
  watchdog('hotel booking', $message);
  if (is_array($order->products)) {
    foreach ($order->products as $key => $product) {
      if ($product->data['module'] = 'hotel_booking') {
        $nid = $product->nid;
        $nights = $product->data['nights'];
        if (is_array($nights)) {
          foreach ($nights as $night) {
            if (hotel_booking_non_zeroed_date(date_make_date($night))) {
              watchdog('hotel booking', t('Room type: @model availability updated for !night', array('@model' => $product->model, '!night' => $night)));
              db_query("UPDATE {hotel_booking_availability_calendars}
                       SET available = available %s 1
                       WHERE nid = %d
                       AND calendar_dt = '%s'", $sign, $nid, $night);
            }
          }
        }
      }
    }
  }
} // end of hotel_booking_alter_availability()