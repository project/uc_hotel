<?php

/**
 * Ubercart Hotel Module
 *
 * @file
 * Install file for Ubercart Hotel module.
 * @author Will Vincent (tcindie) <tcindie at gmail dot com>
 * @author Lee Rowlands (larowlan) <contact at rowlandsgroup dot com>
 */

/**
 * Implementation of hook_schema().
 */

function hotel_booking_schema() {

  $schema = array();
  $schema['hotel_booking_rates'] = array(
    'description' => t('List of booking base rates'),
    'fields' => array(
      'hbrid' => array(
        'description' => t('Hotel booking base rate id'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => t('Booking base rate name'),
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('hbrid'),
  );
  $schema['hotel_booking_occupancy_modifiers'] = array(
    'description' => t('Provides rate modifiers based on occupants'),
    'fields' => array(
      'hbomid' => array(
        'description' => t('Id field'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'threshold' => array(
        'description' => t('Number of occupants after which modifier applies'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => t('Type of occupant A (adults) C (child) or both (B)'),
        'type' => 'varchar',
        'length' => '1',
        'not null' => FALSE,
        'default' => 'A',
      ),
      'rate' => array(
        'description' => t('Modifier rate'),
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => '9',
        'scale' => '3',
      ),
    ),
    'primary key' => array('hbomid'),
    'indexes' => array(
      'hotel_booking_occ_mod_type_ix' => array('type')
    )
  );

  $schema['hotel_booking_rate_modifiers'] = array(
    'description' => t('Basic modifiers to allow  reusable base rates'),
    'fields' => array(
      'hbrmid' => array(
        'description' => t('Id field'),
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'method' => array(
        'description' => t('Type of modifiers P (percent) or V (value)'),
        'type' => 'varchar',
        'length' => '1',
        'not null' => FALSE,
        'default' => 'P',
      ),
      'rate' => array(
        'description' => t('The modifier'),
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => '9',
        'scale' => '3',
      ),
    ),
    'primary key' => array('hbrmid'),
  );

  $schema['hotel_booking_rate_calendars'] = array(
    'description' => t(''),
    'fields' => array(
      'hbrid' => array(
        'description' => t('The base rate id'),
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
      'calendar_dt' => array(
        'description' => t('Date rate is valid'),
        'type' => 'datetime',
        'not null' => FALSE,
      ),
      'rate' => array(
        'description' => t('The rate for this date and base rate'),
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => '9',
        'scale' => '3',
      ),
    ),
  );
  $schema['hotel_booking_availability_calendars'] = array(
    'description' => t('Availability calendars for rooms'),
    'fields' => array(
      'nid' => array(
        'description' => t('Node id of room type'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'calendar_dt' => array(
        'description' => t('Availability date'),
        'type' => 'datetime',
        'not null' => TRUE,
      ),
      'available' => array(
        'description' => t('Number of rooms of this type available for this date'),
        'type' => 'int',
        'unsigned' => FALSE,
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'minimum_occupancy' => array(
        'description' => t('Minimum occupancy for this date'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'size' => 'tiny',
        'default' => 0,
      ),
      'minimum_stay' => array(
        'description' => t('The minimum number of nights required for checking in on this date'),
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'no_check_in' => array(
        'description' => t('Can guests check in at this date'),
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'no_check_out' => array(
        'description' => t('Can guests check out on this date'),
        'size' => 'tiny',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'hotel_booking_avail_cal_min_stay_ix' => array('nid'),
      'hotel_booking_avail_cal_no_check_in_ix' => array('no_check_in'),
      'hotel_booking_avail_cal_no_check_out_ix' => array('no_check_out'),
      'hotel_booking_avail_cal_min_occ_ix' => array('minimum_occupancy'),
      'hotel_booking_avail_cal_cal_dt_ix' => array('minimum_stay'),
      'hotel_booking_avail_cal_nid_ix' => array('nid'),
    ),
  );

  $schema['hotel_booking_room_occupancy_modifiers'] = array(
    'description' => t('Stores association between room types and modifiers'),
    'fields'  => array(
      'nid' => array(
        'description' => t('Room type nid'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'hbomid' => array(
        'description' => t('Occupancy modifier id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'hotel_bkg_room_op_mod_nid_ix' => array('nid'),
    ),
  );

  $schema['hotel_booking_reward_memberships'] = array(
    'description'     => t('Stores member number to order association'),
    'fields'          => array(
      'order_id'      => array(
        'description' => t('Ubercart order id'),
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => '0',
      ),
      'member_number' => array(
        'description' => t('Member number for reward'),
        'type'        => 'varchar',
        'length'      => '255',
        'not null'    => FALSE,
        'default'     => NULL,
      ),
    ),
    'primary key' => array('order_id'),
  );

  $schema['hotel_booking_room_types'] = array(
    'description' => t('Table for Hotel Room Content Type'),
    'fields' => array(
      'nid' => array(
        'description' => t('Hotel content nid'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'vid' => array(
        'description' => t('Hotel content vid'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'capacity' => array(
        'description' => t('Maximum room capacity'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'hbrid' => array(
        'description' => t('Hotel booking rate id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'hbrmid' => array(
        'description' => t('Hotel booking rate modifier id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'smoking' => array(
        'description' => t('Boolean 1=Yes, 0=No'),
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => '0',
      ),
      'minimum_occupancy' => array(
        'description' => t('Default minimum occupancy (can be overriden by date)'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'default_available' => array(
        'description' => t('Default number available'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
      ),
      'model' => array(
        'description' => t('SKU/Model Number of Room Type'),
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
      ),
      'from_rate' => array(
        'description' => t('The min rate for future dates @see hotel_booking_cron'),
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => '6',
        'scale' => '3',
      ),
    ),
    'primary key' => array('vid'),
    'unique keys' => array(
      'hotel_booking_room_types_nid_vid_ix' => array('nid', 'vid'),
      'hotel_booking_room_types_nid_ix' => array('nid'),
    ),
  );
  return $schema;
}


function hotel_booking_update_6100() {
  variable_set('hotel_booking_no_search_results', t('<p>Please select different dates and try your search again.</p>'));
  return array();
}

/**
 * Implementation of hook_install().
 */

function hotel_booking_install() {
  drupal_install_schema('hotel_booking');
  // Ensure default variables are set on install.
  variable_set('hotel_booking_display_nodeview', TRUE);
  variable_set('hotel_booking_display_calprices', TRUE);
  variable_set('hotel_booking_display_monthcount', 3);
  variable_set('hotel_booking_expiration', 'today');
  variable_set('hotel_booking_redirect', 'cart');
  variable_set('hotel_booking_redirect_otherurl', NULL);
  variable_set('hotel_booking_sort_order', 'ASC');
  variable_set('hotel_booking_teaser_in_cart', 0);
  variable_set('hotel_booking_teaser_in_panes', 0);
  variable_set('hotel_booking_reward_prompt', NULL);
  variable_set('hotel_booking_upgrade_product_classes', NULL);
  variable_set('hotel_booking_best_rate', NULL);
  variable_set('hotel_booking_no_search_results', t('<p>Please select different dates and try your search again.</p>'));
  variable_set('hotel_booking_block_children', 1);
  variable_set('hotel_booking_block_smoking', 1);
  variable_set('hotel_booking_search_teaser_body', 'teaser');
  if (function_exists('uc_product_add_default_image_field')) {
    uc_product_add_default_image_field('hotel_room_type');
  }
}

/**
 * Implementation of hook_uninstall().
 */

function hotel_booking_uninstall() {
  drupal_uninstall_schema('hotel_booking');
  variable_del('hotel_booking_display_nodeview');
  variable_del('hotel_booking_display_calprices');
  variable_del('hotel_booking_display_monthcount');
  variable_del('hotel_booking_expiration');
  variable_del('hotel_booking_redirect');
  variable_del('hotel_booking_redirect_otherurl');
  variable_del('hotel_booking_sort_order');
  variable_del('hotel_booking_teaser_in_cart');
  variable_del('hotel_booking_teaser_in_panes');
  variable_del('hotel_booking_reward_prompt');
  variable_del('hotel_booking_upgrade_product_classes');
  variable_del('hotel_booking_best_rate');
  variable_del('hotel_booking_no_search_results');
  variable_del('hotel_booking_block_children');
  variable_del('hotel_booking_block_smoking');
  variable_del('hotel_booking_search_teaser_body');
}

/**
 * Defines the old schema which we are changing
*/
function hotel_booking_schema_old() {
  $schema = array();
  $schema['hotel_booking_room_types'] = array(
    'rate_type' => 'hbrid',
    'rate_modifier' => 'hbrmid',
    'min_occ' => 'minimum_occupancy',
    'avail' => 'default_available',
  );

  $schema['hotel_booking_rates'] = array(
    'hrid' => 'hbrid',
  );

  $schema['hotel_booking_occupancy_modifiers'] = array(
    'hoid' => 'hbomid',
    'adult_child' => 'type',
    'value' => 'rate',
  );

  $schema['hotel_booking_rate_modifiers'] = array(
    'hrmid' => 'hbrmid',
    'value' => 'rate',
  );

  $schema['hotel_booking_rate_calendars'] = array(
    'hrid' => 'hbrid',
    'caldate' => 'calendar_dt',
  );

  $schema['hotel_booking_occupancy_modifiers'] = array(
    'hoid' => 'hbomid',
    'adult_child' => 'type',
    'value' => 'rate',
  );

  $schema['hotel_booking_availability_calendars'] = array(
    'rtid' => 'nid',
    'caldate' => 'calendar_dt',
    'avail_quantity' => 'available',
    'minoccupancy' => 'minimum_occupancy',
    'minstay' => 'minimum_stay',
    'nocheckin' => 'no_check_in',
    'nocheckout' => 'no_check_out',
  );

  $schema['hotel_booking_room_occupancy_modifiers'] = array(
    'hoid' => 'hbomid',
  );


  return $schema;
}

/**
 * Implementation of hook_update_N
 * Update schema to suit new version
 * disable old modules
*/
function hotel_booking_update_6200() {
  $ret = array();

  $table_changes = array(
    'hotel_availability_calendars' => 'hotel_booking_availability_calendars',
    'hotel_occupancy_modifiers' => 'hotel_booking_occupancy_modifiers',
    'hotel_rate_calendars' => 'hotel_booking_rate_calendars',
    'hotel_rate_modifiers' => 'hotel_booking_rate_modifiers',
    'hotel_rate_types' => 'hotel_booking_rates',
    'hotel_room_occupancy_modifiers' => 'hotel_booking_room_occupancy_modifiers',
    'hotel_room_types' => 'hotel_booking_room_types'
  );


  foreach ($table_changes as $old => $new) {
    db_rename_table($ret, $old, $new);
  }

  $new = hotel_booking_schema();
  $old = hotel_booking_schema_old();


  foreach ($new as $table => $schema) {
    if (is_array($old[$table])) {
      foreach ($old[$table] as $old_field => $new_field) {
        db_change_field($ret, $table, $old_field, $new_field, $schema['fields'][$new_field]);
      }
    } 
  }

  //add from_rate field
  db_add_field($ret, 'hotel_booking_room_types', 'from_rate', $new['hotel_booking_room_types']['fields']['from_rate']);

  module_disable(array('hotel_room_type', 'hotel_calendars'));

  $ret[] = update_sql("UPDATE {hotel_booking_occupancy_modifiers} SET type = 'A' WHERE type = 'a'");
  $ret[] = update_sql("UPDATE {hotel_booking_occupancy_modifiers} SET type = 'C' WHERE type = 'c'");

  $ret[] = update_sql("UPDATE {hotel_booking_rate_modifiers} SET method = 'P' WHERE method = 'percent'");
  $ret[] = update_sql("UPDATE {hotel_booking_rate_modifiers} SET method = 'V' WHERE method = 'value'");


  return $ret;
}

/**
 * Implementation of hook_update_N
 * Update old refs in node_type
*/
function hotel_booking_update_6201() {
  $ret = array();
  $qry = "UPDATE {node_type} SET module = 'hotel_booking' WHERE module = 'hotel_room_type'";
  $res = update_sql($qry);
  $ret[] = $res;
  return $ret;
}

/**
 * Implementation of hook_update_N
 * Alter field structures to allow for Yen in rates (precision of 9 required)
*/
function hotel_booking_update_6202() {
  $ret = array();
  foreach (array('hotel_booking_rate_modifiers', 'hotel_booking_rate_calendars',
                 'hotel_booking_occupancy_modifiers') as $table) {
  //change precision to 9
    db_change_field($ret, $table, 'rate', 'rate', array(
      'description' => t('The rate'),
      'type' => 'numeric',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
      'precision' => '9',
      'scale' => '3',
    ));
  }
  return $ret;
}