<?php
/*
 * @file hotel_booking.ubercart.inc
 * Ubercart hooks for hotel_booking module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */
/**
 * Implementation of hook_update_cart_item().
 */
function hotel_booking_update_cart_item($nid, $data = array(), $qty, $cid = NULL) {
    if (!$nid) return NULL;

  $cid = !(is_null($cid) || empty($cid)) ? $cid : uc_cart_get_id();
  if ($qty === 0) {
    // If the Addon module is enabled, remove all the upgrade & addon items from
    // the cart when their parent item is deleted.
    if (module_exists('hotel_addons_upgrades')) {
      hotel_booking_inc('util');
      hotel_booking_remove_upgrades($data['booking_upgrades']);
    }
    uc_cart_remove_item($nid, $cid, $data);
  }
  else {
    db_query("UPDATE {uc_cart_products} SET qty = %d, changed = %d WHERE nid = %d AND cart_id = '%s' AND data = '%s'", $qty, time(), $nid, $cid, serialize($data));
  }

  // Rebuild the items hash
  uc_cart_get_contents(NULL, 'rebuild');
}

/**
 * Implementation of hook_order().
 */
function hotel_booking_order($op, &$order) {
  switch ($op) {
    case 'load':
      hotel_booking_inc('util');
      if (hotel_booking_item_in_order($order) &&
          variable_get('hotel_booking_reward_prompt', '')) {
        $result = db_query('SELECT *
                           FROM {hotel_booking_reward_memberships}
                           WHERE order_id = %d', $order->order_id);
        if ($data = db_fetch_object($result)) {
          $order->reward_membership = $data->member_number;
        }
      }
      break;

    case 'save':
      if (hotel_booking_item_in_order($order) &&
          variable_get('hotel_booking_reward_prompt', '')) {
        if (!empty($order->reward_membership)) {
          $record = array(
            'order_id' => $order->order_id,
            'member_number' => $order->reward_membership
          );
          drupal_write_record('hotel_booking_reward_memberships', $record, array('order_id'));
        }
      }
      break;

    case 'delete':
      db_query('DELETE FROM {hotel_booking_reward_memberships}
               WHERE order_id = %d', $order->order_id);
      break;
  }
}


/**
 * Implementation of hook_cart_display().
 */
function hotel_booking_cart_display($item) {
  $node = node_load($item->nid);
  $element = array();
  $element['nid'] = array('#type' => 'value', '#value' => $node->nid);
  $element['module'] = array('#type' => 'value', '#value' => 'hotel_booking');
  $element['remove'] = array('#type' => 'checkbox');

  $title = check_plain($item->title);
  $title .= ' ('. count($item->data['nights']) . t(' Nights, ');
  $title .= format_plural($item->data['adults'], '1 Adult', '@count Adults');
  $title .= format_plural($item->data['children'], ', 1 Child', ', @count Children');
  $title .= ')';
  $element['title'] = array(
    '#value' => node_access('view', $node) ? l($title, 'node/'. $node->nid) : check_plain($item->title),
  );

  $context = array(
    'revision' => 'altered',
    'location' => 'cart-item',
    'subject' => array(
      'cart_item' => $item,
    ),
    'extras' => array(
      'node' => $node,
    ),
  );
  $price_info = array(
    'price' => $item->price,
    'qty' => $item->qty,
  );

  $element['#total'] = uc_price($price_info, $context);
  $element['data'] = array('#type' => 'hidden', '#value' => serialize($item->data));
  $element['qty'] = array(
    '#type'          => 'markup',
    '#value'         => 1,
    '#default_value' => 1,
  );

  if (variable_get('hotel_booking_teaser_in_cart', FALSE)) {
    $description = theme('hotel_booking_room_cart_description', $node->teaser, NULL); //$item->data);
  }
  else {
    $description = theme('hotel_booking_room_cart_description', '', NULL); //$item->data);
  }
  if ($description) {
    $element['description'] = array('#value' => $description);
  }

  return $element;
}

/**
 * Implementation of hook_cart_item().
 */
function hotel_booking_cart_item($op, &$item) {
  switch ($op) {
    case 'load':
      if ($item->module == 'hotel_booking') {
        $data = $item->data;
        $item->price = $data['total'];
        $item->qty = 1;
      }
      break;

    case 'remove':
      $data = unserialize($item->data);
      if ($data['module'] == 'hotel_booking') {
        $item->price = $data['total'];
        $item->qty = 1;

        // Fire item_removed trigger:
        $data['nid'] = $item->nid; //add the nid so we've got it in the trigger
        ca_pull_trigger('item_removed_from_cart', $data);
      }
      break;
  }
}

/**
 * Implementation of hook_cart_item_description().
 */
function hotel_booking_cart_item_description($item) {
  $node = node_load($item->nid);
  return theme('hotel_booking_room_cart_description', $node->teaser, $item->data);
}

/**
 * ----------------------------------------
 * Cart, Checkout, and Order Pane functions
 * ----------------------------------------
 */


/**
 * Implementation of hook_cart_pane().
 */
function hotel_booking_cart_pane() {
  hotel_booking_inc('util');
  if (hotel_booking_item_in_cart()) {
    $panes[] = array(
      'id' => 'booking_details',
      'title' => t('Booking Details'),
      'weight' => -15,
      'body' => drupal_get_form('hotel_booking_details_pane_cart'),
      'collapsible' => TRUE,
      'collapsed'   => FALSE,
    );
    return $panes;
  }
}

/**
 * Implementation of hook_checkout_pane().
 */
function hotel_booking_checkout_pane() {
  hotel_booking_inc('util');
  if (hotel_booking_item_in_cart()) {
    if (variable_get('hotel_booking_reward_prompt', '')) {
      $panes[] = array(
        'id'          => 'reward_membership',
        'callback'    => 'hotel_booking_rewards',
        'title'       => t('Reward Membership'),
        'weight'      => 6,
        'enabled'     => TRUE,
        'process'     => TRUE,
        'collapsible' => FALSE,
      );
    }
    $panes[] = array(
      'id'            => 'booking_details',
      'callback'      => 'hotel_booking_details_pane_checkout',
      'title'         => t('Booking Details'),
      'weight'        => -8,
      'enabled'       => TRUE,
      'collapsible'   => TRUE,
      'process'       => TRUE,
    );
    return $panes;
  }
}

/**
 * Implementation of hook_order_pane().
 */
function hotel_booking_order_pane() {
  $panes[] = array(
    'id' => 'hotel_booking',
    'callback' => 'hotel_booking_booking_pane',
    'title' => t('Booking Details'),
    'desc' => t('Display details of room bookings.'),
    'class' => 'abs-left',
    'weight' => 5,
    'show' => array('view', 'customer'),
  );
  if (variable_get('hotel_booking_reward_prompt', '')) {
    $panes[] = array(
      'id' => 'reward_membership',
      'callback' => 'hotel_booking_pane_rewards',
      'title' => t('Reward Membership'),
      'weight' => 5,
      'show' => array('view', 'customer'),
    );
  }
  return $panes;
}