<?php

/**
 * Ubercart Hotel Booking module.
 *
 * @file
 * Provides a hotel room type node type that pricing and availability information can be tied to.
 * Originally based on Availability Calendars module by Dan Karran (geodaniel).
 * @author Will Vincent (tcindie) <tcindie at gmail dot com>
 * @author Lee Rowlands (larowlan) <contact at rowlandsgroup dot com>
 */

/**
 * Load the includes
 * Done this way for ease of maintenance and readability
 * Multiple files instead of multiple modules b/c as multiple
 * modules - each modules depends on the other - hence
 * should be 1 module
 *
*/
hotel_booking_inc('ca'); //ca hooks
hotel_booking_inc('ubercart'); //ubercart hooks
hotel_booking_inc('token'); //token hooks
hotel_booking_inc('room_type'); //room type node hooks
hotel_booking_inc('panes'); //Ubercart panes

/**
 * Core Hooks
 * -------------------------------------------------------------------------------------
*/
/**
 * Implementation of hook_init
*/
function hotel_booking_init() {
  //allow translation of the no search results variable
  global $conf;
  $conf['i18n_variables'][] = 'hotel_booking_no_search_results';
}

/**
 * Implementation of hook_perm().
 */
function hotel_booking_perm() {
  return array(
    'edit own hotel room availability',
    'edit any hotel room availability',
    'administer hotel booking settings',
    'view hotel search results',
    'create hotel room types',
    'edit all hotel room types',
    'delete hotel room types',
    'edit own hotel room type'
  );
}

/**
 * Implementation of hook_menu().
 */
function hotel_booking_menu() {
  $items = array();

  $items['admin/store/hotel_booking'] = array(
    'title'            => 'Hotel Booking',
    'description'      => t('Administer Hotel Booking settings, configure base rate(s), rate and occupancy modifiers, and add-on/upgrade products.'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rates_list_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_NORMAL_ITEM,
    'file'             => 'hotel_booking.admin.inc',
  );
  $items['admin/store/hotel_booking/rates'] = array(
    'title'            => 'Base Rates',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rates_list_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_DEFAULT_LOCAL_TASK,
    'weight'           => 0,
    'file'             => 'hotel_booking.admin.inc',
  );
  $items['admin/store/hotel_booking/rates/add'] = array(
    'title'            => 'Add Rate',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rates_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/rates/%hotel_booking_rate'] = array(
    'title'            => 'View Rate',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rates_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/rates/%hotel_booking_rate/delete'] = array(
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rates_delete_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/rate_modifiers'] = array(
    'title'            => 'Rate Modifiers',
    'page arguments'   => array('hotel_booking_rate_modifiers_list_form'),
    'page callback'    => 'drupal_get_form',
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 1,
    'file'             => 'hotel_booking.admin.inc',
  );
  $items['admin/store/hotel_booking/rate_modifiers/add'] = array(
    'title'            => 'Add Rate',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rate_modifiers_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/rate_modifiers/%hotel_booking_rate_modifier'] = array(
    'title'            => 'View Rate',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rate_modifiers_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/rate_modifiers/%hotel_booking_rate_modifier/delete'] = array(
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_rate_modifiers_delete_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/occupancy_modifiers'] = array(
    'title'            => 'Occupancy Modifiers',
    'page arguments'   => array('hotel_booking_occupancy_modifiers_list_form'),
    'page callback'    => 'drupal_get_form',
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 2,
    'file'             => 'hotel_booking.admin.inc',
  );
  $items['admin/store/hotel_booking/occupancy_modifiers/add'] = array(
    'title'            => 'Add Occupancy Modifier',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_occupancy_modifiers_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/occupancy_modifiers/%hotel_booking_occupancy_modifier'] = array(
    'title'            => 'View Occupancy Modifier',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_occupancy_modifiers_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/occupancy_modifiers/%hotel_booking_occupancy_modifier/delete'] = array(
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_occupancy_modifiers_delete_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_booking.admin.inc'
  );
  $items['admin/store/hotel_booking/hotel_settings'] = array(
    'title'            => 'Settings',
    'description'      => 'Configure global settings for Hotel Booking module.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_booking_admin_settings'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 4,
    'file'             => 'hotel_booking.admin.inc',
  );
  $items['node/%node/availability'] = array(
    'title'           => 'Hotel Availability Calendar',
    'page callback'   => 'drupal_get_form',
    'page arguments'  => array('hotel_booking_availability_form', 1),
    'file'            => 'hotel_booking.calendars.inc',
    'access arguments'  => array(1),
    'access callback' => 'hotel_booking_availability_edit_access',
    'type'            => MENU_LOCALTASK,
  );
  $items['hotel-booking/search'] = array(
    'title'           => 'Room search',
    'page callback'   => 'drupal_get_form',
    'access arguments' => array('access content'),
    'page arguments' => array('hotel_booking_search_form', TRUE),
    'type'            => MENU_SUGGESTED_ITEM,
  );

  return $items;
}


/**
 * Implementation of hook_theme().
 */
function hotel_booking_theme() {
  return array('hotel_booking_night_list' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments'  => array(
        'nights'   => array(),
        'prices'   => array(),
        'adults'   => int,
        'children' => int,
        'html'     => FALSE,
      ),
    ),
    'hotel_booking_search_result_price' => array(
      'template' => 'hotel_booking_search_result_price',
      'arguments' => array(
        'caption'  => NULL,
        'price' => NULL,
        'class' => NULL
      ),
    ),
    'hotel_booking_room_cart_description' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'teaser'  => '',
        'data'    => array()
      ),
    ),
    //themes for rate lists
    //these are builts as forms
    //and themed as tables
    //to allow other modules to modify them w/ hook_form_alter
    'hotel_booking_rates_list_form' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'form'  => NULL,
      ),
    ),
    'hotel_booking_rate_modifiers_list_form' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'form'  => NULL,
      ),
    ),
    'hotel_booking_occupancy_modifiers_list_form' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'form'  => NULL,
      ),
    ),
    'hotel_booking_search_form' => array(
      'template' => 'hotel_booking_search_form',
      'arguments' => array(
        'form' => NULL
      )
    ),
    'hotel_booking_search_results' => array(
      'template' => 'hotel_booking_search_results',
      'arguments' => array(
        'form' => NULL
      )
    ),
    'hotel_booking_rates_form_calendar' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'form' => NULL
      )
    ),
    'hotel_booking_availability_form' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'form' => NULL
      )
    ),
    'hotel_booking_calendar_cell' => array(
      'template' => 'hotel_booking_calendar_cell',
      'arguments' => array(
        'day' => NULL
      )
    ),
    'hotel_booking_calendar_cell_price' => array(
      'template' => 'hotel_booking_calendar_cell_price',
      'arguments' => array(
        'day' => NULL
      )
    ),
    'hotel_booking_pane_help' => array(
      'template' => 'hotel_booking_pane_help',
      'arguments' => array(
        'product' => NULL,
        'node' => NULL
      )
    ),
    'hotel_booking_calendar_cell_tip' => array(
      'template' => 'hotel_booking_calendar_cell_tip',
      'arguments' => array(
        'day' => NULL
      )
    ),
    'hotel_booking_calendars' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'node' => NULL,
        'results' => NULL
      ),
    ),
    'hotel_booking_calendars_month' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'node' => NULL,
        'year' => NULL,
        'month' => NULL,
        'days' => NULL
      ),
    ),
    'hotel_booking_calendars_months' => array(
      'file' => 'hotel_booking.theme.inc',
      'arguments' => array(
        'node' => NULL,
        'year' => NULL,
        'month' => NULL
      ),
    ),
    'hotel_booking_availability_key' => array(
      'template' => 'hotel_booking_availability_key',
      'arguments' => array(),
    ),
  );
}

/**
 * Implementation of hook_block().
 */
function hotel_booking_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[] = array(
        'info' => t('Hotel Booking: Search form'),
      );
      return $blocks;

    case 'configure':
      $form = array();
      // Build list of room types
      $room_types = array(t('None'));
      $results = db_query('
        SELECT hbrt.nid,
          n.title
        FROM {hotel_booking_room_types} AS hbrt
        INNER JOIN {node} AS n
        ON n.vid = hbrt.vid'
      );
      while ($result = db_fetch_array($results)) {
        $room_types['node/'. $result['nid']] = check_plain($result['title']);
      }

      $form['best_rate_link'] = array(
        '#type'          => 'select',
        '#title'         => t('Room with best rate'),
        '#default_value' => variable_get('hotel_booking_best_rate', '0'),
        '#options'       => $room_types,
        '#description'   => t("Select the room type with the best rate to display a link to it in the search block, if no room type is selected, the link will not be displayed."),
      );

      $form['search_children'] = array(
        '#type'          => 'select',
        '#title'         => t('Children Selection'),
        '#default_value' => variable_get('hotel_booking_block_children', '1'),
        '#options'       => array('No', 'Yes'),
        '#description'   => t('Select whether or not to display the children selection option in search block. Value will be 0 for searches if No is selected here.'),
      );

      $form['search_smoking'] = array(
        '#type'          => 'select',
        '#title'         => t('Smoking Preference Selection'),
        '#default_value' => variable_get('hotel_booking_block_smoking', '1'),
        '#options'       => array('No', 'Yes'),
        '#description'   => t('Select whether or not to display the smoking preference selection option in search block. Value will be No Preference if No is selected here.'),
      );
      return $form;


    case 'save':
      variable_set('hotel_booking_best_rate', $edit['best_rate_link']);
      variable_set('hotel_booking_block_children', $edit['search_children']);
      variable_set('hotel_booking_block_smoking', $edit['search_smoking']);
      break;


    case 'view':
      drupal_add_css(drupal_get_path('module', 'hotel_booking') .'/css/hotel_booking_block.css');
      $block['subject'] = t('Room Search');
      $block['content'] = drupal_get_form('hotel_booking_search_form');
      return $block;
  }
}

/**
 * Implementation of hook_form_alter().
 *
 * Since Hotel Rooms will never be shippable, hide the shipping fieldset from the form if uc_shipping is enabled.
 */
function hotel_booking_form_alter(&$form, $form_state, $form_id) {
  if ($form['type']['#value'] == 'hotel_room_type'  && module_exists('uc_shipping')) {
    unset($form['shipping']);
  }
  return;
}

/**
 * Implementation of hook_cron().
 */
function hotel_booking_cron() {
  hotel_booking_inc('util');
  $expiration_dt = hotel_booking_expiration_date();
  db_query("UPDATE {hotel_booking_availability_calendars}
           SET available = 0
           WHERE calendar_dt <= '%s'", $expiration_dt->format(DATE_FORMAT_DATE));
  $res = db_query("SELECT nid FROM {hotel_booking_room_types}");
  while ($room = db_fetch_object($res)) {
    hotel_booking_set_room_from_rate($room->nid);
  }
  hotel_booking_auto_availability();
}

/**
 * Implementation of hook_enable
*/
function hotel_booking_enable() {
  //this has to run here in case uc_product is enabled at the same time as hotel_booking
  if (function_exists('uc_product_add_default_image_field')) {
    uc_product_add_default_image_field('hotel_room_type');
  }
}

/**
 * Implementation of hook_views_api().
 */
function hotel_booking_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'hotel_booking'),
  );
}

/**
 * Implementation of hook_imagecache_default_presets
*/
function hotel_booking_imagecache_default_presets() {
  $presets = array();

  $presets['hotel_booking_search_result'] = array(
    'presetname' => 'hotel_booking_search_result',
    'actions' =>
    array(
      0 =>
      array(
        'weight' => '0',
        'module' => 'imagecache',
        'action' => 'imagecache_scale_and_crop',
        'data' =>
        array(
          'width' => '216',
          'height' => '162',
          'upscale' => 0,
        ),
      ),
    ),
  );
  return $presets;
}

/**
 * Access callbacks
 * -------------------------------------------------------------------------------------
*/
/**
 * Access callback for hotel calendars
 * @param $node object node being edited
*/
function hotel_booking_availability_edit_access($node) {
  global $user;
  return ($node->type == 'hotel_room_type' &&
          (user_access('edit any hotel room availability') ||
           (user_access('edit own hotel room availability') && $node->uid == $user->uid)));
}
/**
 * Contrib module hooks
 * -------------------------------------------------------------------------------------
*/

/**
 * Implementation of hook_modify_room_rate
*/
function hotel_booking_modify_room_rate($price, $node, $date,
                                        $adults, $children, $smoking,
                                        $check_in, $check_out, $reset = FALSE) {
  //rate modifiers
  static $rate_modifier = array();
  if (!count($rate_modifier) || $reset) {
    $rate_modifier = array();
    $rate_modifier_res = db_query('SELECT method, rate
                            FROM {hotel_booking_rate_modifiers} hbrm
                            INNER JOIN {hotel_booking_room_types} hbrt
                              ON hbrt.hbrmid = hbrm.hbrmid
                            WHERE hbrt.vid = %d', $node->vid);
    $rate_modifier = db_fetch_array($rate_modifier_res);
  }
  if ($rate_modifier['method'] == 'V') { //value
    $price += $rate_modifier['rate'];
  }
  else {
    $price += ($price * ($rate_modifier['rate']/100));
  }
  //fetch occupancy modifiers
  static $occupancy_modifiers = array();
  $occupants = $adults + $children;
  $variable_map = array(
    'A' => 'adults',
    'C' => 'children',
    'B' => 'occupants'
  );
  if (!count($occupancy_modifiers) || $reset) {
    //reset
    $occupancy_modifiers = array();
    $occupancy_modifiers_res = db_query('SELECT o.*
                          FROM {hotel_booking_occupancy_modifiers} AS o
                          INNER JOIN {hotel_booking_room_occupancy_modifiers} AS ro
                          ON ro.nid = %d AND ro.hbomid = o.hbomid',
                          $node->nid, $adults);
    while ($occupancy_modifier = db_fetch_object($occupancy_modifiers_res)) {
      $variable = $variable_map[$occupancy_modifier->type];
      if ($$variable > $occupancy_modifier->threshold) {
        $surplus = $$variable - $occupancy_modifier->threshold;
        $price += ($surplus * $occupancy_modifier->rate);
        /*reduce this so not applied again if multiple occupancy
         modifiers for this type that are yet to be processed*/
        $$variable -= $surplus;
      }
    }
  }

  return $price;
}

/**
 * Form builders/handlers
 * -------------------------------------------------------------------------------------
*/

/**
 * hotel booking search block content.
 * @param $form_state array std FAPI form_state variable
 * @param $page boolean TRUE if being shown on a page, FALSE for block
 */
function hotel_booking_search_form($form_state, $page = FALSE) {
  $form = array();
  if (!$page) {
    $form['#action'] = url('hotel-booking/search');
  }
  //store form state for theme functions
  $form['page'] = array(
    '#type' => 'value',
    '#value' => $page
  );

  hotel_booking_inc('util');
  $defaults = hotel_booking_defaults($form_state);

  //$form['#action'] = url('hotel-booking/search');

  if (variable_get('hotel_booking_best_rate', '')) {
    $form['search']['link'] = array(
      '#value'  => l(t('View Availability of Best Room Rate'), variable_get('hotel_booking_best_rate', '')),
    );
  }

  $form['search']['check_in'] = array(
    '#type' => 'date_popup',
    '#title' => t('Check-In Date'),
    '#required' => TRUE,
    '#size' => 14,
    '#default_value' => $defaults['check_in'],
    '#date_type' => DATE_ISO,
    '#date_timezone' => date_default_timezone_name(),
    '#date_format' => variable_get('uc_date_format_default', 'd M Y'),
    '#date_year_range' => '-0:+1',
  );
  $form['search']['nights'] = array(
    '#type' => 'select',
    '#title' => t('Nights'),
    '#options' => drupal_map_assoc(range(1, 14)),
    '#default_value' => $defaults['nights'],
  );
  $form['search']['adults'] = array(
    '#type' => 'select',
    '#title' => t('Adults'),
    '#options' => drupal_map_assoc(range(1, 8)),
    '#default_value' => $defaults['adults'],
  );
  if (variable_get('hotel_booking_block_children', 1) || $page) {
    $form['search']['children'] = array(
      '#type' => 'select',
      '#title' => t('Children'),
      '#options' => range(0, 8),
      '#default_value' => $defaults['children']
    );
  }
  else {
    $form['search']['children'] = array(
      '#type' => 'value',
      '#value' => $defaults['children'],
    );
  }
  if (variable_get('hotel_booking_block_smoking', 1)) {
    $form['search']['smoking'] = array(
      '#type' => 'select',
      '#title' => t('Preference'),
      '#options' => array(
        '1' => t('Non-Smoking'),
        '2' => t('Smoking'),
        '3' => t('No Preference'),
      ),
      '#default_value' => $defaults['smoking'],
    );
  }
  else {
    $form['search']['smoking'] = array(
      '#type' => 'value',
      '#value' => 3,
    );
  }

  $form['search']['submit'] = array(
    '#type'   => 'submit',
    '#title'  => t('Search'),
    '#value'  => t('Search Rooms'),
  );

  if ($form_state['submitted']) {
    $form['results'] = array(
      '#tree' => TRUE,
      '#theme' => 'hotel_booking_search_results',
      '#submitted' => TRUE //we pass this to preprocessor to stop no rooms on block
    );
    $form['results']['rooms'] = hotel_booking_search_results($form_state['values']);
    unset($form['#action']);
  }
  return $form;

}

/**
 * Function to build search results form
 * @param $values array submitted form values
*/
function hotel_booking_search_results($values) {
  //nicer vars
  $check_in = $values['check_in'];
  $check_in_date = date_make_date($check_in);
  $nights = $values['nights'];
  $adults = $values['adults'];
  $children = $values['children'];
  $smoking = (variable_get('hotel_booking_display_smoking', TRUE)) ? $values['smoking'] : 3;

  hotel_booking_inc('util');
  $rooms = array();

  $check_out = hotel_booking_calculate_checkout($check_in, $nights);

  $visitors = $adults + $children;

  $rooms = hotel_booking_get_available_rooms(
    $check_in,
    $check_out,
    $nights,
    $adults,
    $children,
    $smoking
  );
  $results = hotel_booking_calculate_prices(
    $check_in,
    $check_out,
    $rooms,
    $adults,
    $children,
    $smoking
  );

  $sorted_rates = $results['totals'];
  $results = $results['results'];

  // Sort results by rate. Default to Ascending if no preference is set.
  if (variable_get('hotel_booking_sort_order', 'ASC') == 'ASC') {
    asort($sorted_rates);
  }
  else {
    arsort($sorted_rates);
  }

  $form = array();
  foreach ($sorted_rates as $nid => $total) {
    $room = array();
    $node = node_load($nid);
    $room['nid'] = array(
      '#type' => 'value',
      '#value' => $nid
    );
    /*shove this in here for other modules if they feel so inclined
     node_load is cached anyhow but this makes the code cleaner
    */
    $room['node'] = array(
      '#type' => 'value',
      '#value' => $node
    );
    $room['total'] = array(
      '#type' => 'value',
      '#value' => $total
    );
    $context = array(
      'subject' => array(
        'node' => $node
      ),
      'revision' => 'altered'
    );
    //get the altered price (vat/gst/multicurrency etc)
    $total = uc_price($total, $context);
    //now format it
    $context['revision'] = 'formatted';
    $room['pricing'] = array(
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#title'        => t('Pricing details'),
      '#collapsed'    => TRUE
    );
    $room['display_total'] = array(
      '#value' => theme('hotel_booking_search_result_price', NULL, uc_price($total, $context))
    );
    //this is an altered price so we can then do display
    $average = $total / $nights;
    $per_person = $total / $visitors;
    $room['pricing']['per_person'] = array(
      '#value' => theme('hotel_booking_search_result_price', t('Total / Person'), uc_price($per_person, $context), 'hotel-booking-price-per-person')
    );
    $room['pricing']['display_total'] = array(
      '#value' => theme('hotel_booking_search_result_price', t('Room Total'), uc_price($total, $context), 'hotel-booking-price-total')
    );
    $room['pricing']['average'] = array(
      '#value' => theme('hotel_booking_search_result_price', t('Avg / Night'), uc_price($average, $context), 'hotel-booking-price-average')
    );
    if (variable_get('hotel_booking_calendar_in_search_results', 1)) {
      hotel_booking_inc('calendars');
      $room['calendar'] = array(
        '#type'         => 'fieldset',
        '#collapsible'  => TRUE,
        '#title'        => t('Availability'),
        '#collapsed'    => TRUE
      );
      $room['calendar']['calendars'] = array(
        '#value' => hotel_booking_calendars_node(
          $node,
          $check_in_date->format('Y'),
          $check_in_date->format('n'),
          3
        )
      );
    }
    $room['book'] = array(
      '#attributes' => array('class' => 'book-btn', 'title' => t('Click here to book this room.')),
      '#type' => 'submit',
      '#name' => 'book_'. $nid,
      '#value' => variable_get('hotel_booking_book_room_button_text', t('Book now'))
    );
    if (is_array($results[$nid]['prices']) && count($results[$nid]['prices']) > 0) {
      //nightly breakdown
      $night = 1;
      foreach ($results[$nid]['prices'] as $date => $price) {
        //price alterers
        $context['revision'] = 'altered';
        $price = uc_price($price, $context);
        $room['rates'][$date] = array(
          '#type' => 'value',
          '#value' => $price
        );
        //now the formatter
        $context['revision'] = 'formatted';
        $ref_dt = date_make_date($date);
        $room['pricing']['display_rates'][$date] = array(
          '#value' => theme('hotel_booking_search_result_price', $ref_dt->format('d/m'), uc_price($price, $context), 'hotel-booking-price-nightly hotel-booking-price-'. $night)
        );
        $night++;
      }
    }
    $form[$nid] = $room;
  }

  drupal_add_css(drupal_get_path('module', 'hotel_booking') .'/hotel_booking.css');
  return $form;
}

/**
 * Submit handler for hotel_booking_search_form
*/
function hotel_booking_search_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#parents'] &&
      is_array($form_state['clicked_button']['#parents']) &&
      $form_state['clicked_button']['#parents'][1] == 'rooms' &&
      $form_state['clicked_button']['#parents'][3] == 'book' &&
      is_numeric($form_state['clicked_button']['#parents'][2]) &&
      ($nid = $form_state['clicked_button']['#parents'][2]) &&
      ($node = node_load($nid))
      && $node->type == 'hotel_room_type') {
    $prices = $nights = array();
    foreach ($form_state['values']['results']['rooms'][$nid]['rates'] as $date => $price) {
      $nights[] = $date;
      $prices[]= $price;
    }
    $total = $form_state['values']['results']['rooms'][$nid]['total'];
    $adults = $form_state['values']['adults'];
    $children = $form_state['values']['children'];
    $data = array(
      'module' => 'hotel_booking',
      'prices' => $prices,
      'nights' => $nights,
      'total' => $total,
      'adults' => $adults,
      'children' => $children,
    );

    $redirect_destination = variable_get('hotel_booking_redirect', 'cart');
    uc_cart_add_item($node->nid, 1, $data);
    drupal_set_message(t('Added your booking to the cart, please note that your booking is not
                         confirmed until you <a href="/cart/checkout">Complete checkout</a>'));
    $cart_item_id = db_last_insert_id('{uc_cart_products}', 'cart_item_id');

    if ($redirect_destination == 'cart') {
      drupal_goto($redirect_destination);
    }
    elseif ($redirect_destination == 'addon') {
      drupal_goto('booking_upgrades/'. $cart_item_id);
    }
    else {
      drupal_goto(variable_get('hotel_booking_redirect_otherurl', ''));
    }
  }
  else {
    $form_state['rebuild'] = TRUE;
    hotel_booking_inc('util');
    hotel_booking_save_defaults($form_state);
  }
}

/**
 * Utils
 * -------------------------------------------------------------------------------------
*/
/**
 * Util to load include for the module
 * @param $file string, file fragment - to load hotel_booking.admin.inc pass admin
*/
function hotel_booking_inc($file) {
  module_load_include('inc', 'hotel_booking', 'hotel_booking.'. $file);
}

/**
 * Template preprocess functions to setup variables for templates
 * --------------------------------------------------------------------------------
*/

/**
 * preprocess function for hotel_booking_search_results
*/
function template_preprocess_hotel_booking_search_results(&$vars) {
  $vars['rooms'] = array();
  foreach (element_children($vars['form']['rooms']) as $nid) {
    $room = $vars['form']['rooms'][$nid];
    $node = node_load($nid);
    if (($field = variable_get('uc_image_'. $node->type, '')) && isset($node->$field) && file_exists($node->{$field}[0]['filepath'])) {
      $image = $node->{$field}[0];
      $image = array('#value' => l(theme('imagecache', 'hotel_booking_search_result', $image['filepath'], $image['alt'], $image['title']), drupal_get_path_alias('node/'. $node->nid), array('html' => TRUE)));
    }
    else {
      $image = array('#value' => t('n/a'));
    }
    if (variable_get('hotel_booking_search_teaser_body', 'teaser') == 'teaser') {
      $description = check_markup($node->teaser, $node->format, FALSE) .' '. l(t('more'), drupal_get_path_alias('node/'. $node->nid));
    }
    elseif (variable_get('hotel_booking_search_teaser_body', 'teaser') == 'body') {
      $description = check_markup($node->body, $node->format, FALSE);
    }
    else {
      $description = '';
    }
    $calendar = '';
    if (variable_get('hotel_booking_calendar_in_search_results', 1)) {
      $calendar = drupal_render($room['calendar']);
    }
    $data = array(
      'image' => drupal_render($image),
      'title' => l($node->title, drupal_get_path_alias('node/'. $node->nid)),
      'total' => drupal_render($room['display_total']),
      'book' => drupal_render($room['book']),
      'pricing' => drupal_render($room['pricing']),
      'description' => $description,
      'calendar' => $calendar
    );
    $vars['rooms'][$nid] = $data;
  }
  if (!count($vars['rooms']) && $vars['form']['#submitted']) {
    $vars['no_results'] = variable_get('hotel_booking_no_search_results', '<p>Please select different dates and try your search again.</p>');
  }
  $vars['book_caption'] = variable_get('hotel_booking_book_room_button_text', t('Book Room'));
}

/**
 * preprocess function for hotel_booking_search_form
*/
function template_preprocess_hotel_booking_search_form(&$vars) {
  $form = $vars['form'];
  drupal_add_css(drupal_get_path('module', 'hotel_booking') .'/css/hotel_booking_search.css');

  $vars['check_in'] = drupal_render($vars['form']['search']['check_in']);
  $vars['nights'] = drupal_render($vars['form']['search']['nights']);
  $vars['adults'] = drupal_render($vars['form']['search']['adults']);
  $vars['submit'] = drupal_render($vars['form']['search']['submit']);
  if ($vars['form']['link']) {
    $vars['link'] = drupal_render($vars['form']['search']['link']);
  }
  if ($vars['form']['results']) {
    $vars['results'] = drupal_render($vars['form']['results']);
  }
  if ($vars['form']['search']['smoking']) {
    $vars['smoking'] = drupal_render($vars['form']['search']['smoking']);
  }
  if ($vars['form']['search']['children']) {
    $vars['children'] = drupal_render($vars['form']['search']['children']);
  }
  if ($vars['form']['page'] && $vars['form']['page']['#value']) {
    $vars['template files'][] = 'hotel_booking_search_form_page.tpl.php';
  }
  else {
    $vars['template files'][] = 'hotel_booking_search_form_block.tpl.php';
  }
  $vars['page'] = FALSE;
  if (isset($vars['form']['page']) && $vars['form']['page']['#value']) {
    $vars['page'] = TRUE;
  }
}

/**
 * Preprocess function for hotel_booking_calendar_cell
*/
function template_preprocess_hotel_booking_calendar_cell(&$vars) {
  $vars = array_merge($vars, $vars['day']);
}

/**
 * Preprocess function for hotel_booking_calendar_cell_tip
*/
function template_preprocess_hotel_booking_calendar_cell_tip(&$vars) {
  $vars = array_merge($vars, $vars['day']);
}

/**
 * Preprocess function for hotel_booking_calendar_cell_price
*/
function template_preprocess_hotel_booking_calendar_cell_price(&$vars) {
  $vars = array_merge($vars, $vars['day']);
}

/**
 * Preprocess function for hotel_booking_pane_help
*/
function template_preprocess_hotel_booking_pane_help(&$vars) {
  $arg = arg();
  $vars['cart'] = (count($arg) == 1 && $arg[0] == 'cart');
}

/**
 * Preprocess function for hotel_booking_search_result_price
*/
function template_hotel_booking_search_result_price(&$vars) {
  if (!$vars['class']) {
    $vars['class'] = 'hotel-booking-price';
  }
}

/**
 * Dynamic menu load functions
 * ----------------------------------------------------------------------------
*/
/**
 * Load a hotel booking rate
 * @param $id int id of rate
*/
function hotel_booking_rate_load($id) {
  $rate = db_fetch_object(db_query("SELECT *
                                  FROM {hotel_booking_rates}
                                  WHERE hbrid = %d", $id));
  $oldest = mktime(0, 0, 0, date('n'), 1, date('Y')); //don't load anything older than first day of this month

  $res = db_query("SELECT rate, calendar_dt
                  FROM {hotel_booking_rate_calendars}
                  WHERE hbrid = %d AND
                  calendar_dt >= '%s'",
                  $id, date(DATE_FORMAT_DATE, $oldest));
  $rate->rates = array();
  while ($day = db_fetch_object($res)) {
    list($date, $time) = explode(' ', $day->calendar_dt);
    $rate->rates[$date] = $day->rate;
  }
  return $rate;
}


/**
 * Load a hotel booking rate modifier
 * @param $id int id of rate
*/
function hotel_booking_rate_modifier_load($id) {
  return db_fetch_object(db_query('SELECT *
                                  FROM {hotel_booking_rate_modifiers}
                                  WHERE hbrmid = %d', $id));
}

/**
 * Load a hotel booking occupancy modifier
 * @param $id int id of rate
*/
function hotel_booking_occupancy_modifier_load($id) {
  return db_fetch_object(db_query('SELECT *
                                  FROM {hotel_booking_occupancy_modifiers}
                                  WHERE hbomid = %d', $id));
}


